﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_Clases
{
    class Profesor
    {
        protected string materia;
        protected int horas;
        protected string tema;
        protected string equipo;
        protected string dificultad;

        public Profesor(string mat, int hor, string tem, string equip, string dif)
        {
            materia = mat;
            if (mat!="fisica" && mat != "castellano" && mat != "filosofia" && mat != "programacion" && mat != "ingles" && mat != "proyectos" && mat != "matematica" && mat != "fol")
            {
                throw new FormatException("La materia colocada no es enseña");
            }
            horas =hor;
            if (hor>6)
            {
                throw new Exception("No es legal tanta horas para una materia");
            }
            tema =tem;
            if (tem!="tema 1" && tem != "tema 2" && tem != "tema 3" && tem != "tema 4" && tem != "tema 5" && tem != "tema 6")
            {
                throw new Exception("Ese tema no se dara en este año");
            }
            equipo =equip;
            if (equip != "cuaderno" && equip != "pc" && equip != "libro" && equip != "pensamiento" && equip != "exposiciones" && equip != "videos")
            {
                throw new Exception("El equipo colocado no existe");
            }
            dificultad = dif;
            if (dif!="facil" && dif!="dificil" && dif!="normal" && dif != "llorar")
            {
                throw new Exception("La difilcutad puesta no se entiende");
            }
        }

        public void alago(string algo)
        {
            Console.WriteLine("este profes@r es " + algo);
        }

        public virtual void clase()
        {
            Console.WriteLine("Señor@s porfavor silecio que va a empezar la clase");
        }

        public override string ToString()
        {
            return "El prof. de " + materia + " da " + horas + " h al dia, y esta con el " + tema + ", lo que usamos en sus clases son " + equipo + " y sentimos que su clase es " + dificultad;
        }

        public void setMateria(string mat2)
        {
            materia = mat2;
        }
        public string getMateria()
        {
            return materia;
        }

        public void setHoras(int hor2)
        {
            horas = hor2;
        }
        public int getHoras()
        {
            return horas;
        }

        public void setTema(string tem2)
        {
            tema = tem2;
        }
        public string getTema()
        {
            return tema;
        }

        public void setEquipo(string eq)
        {
            equipo = eq;
        }
        public string getEquipo()
        {
            return equipo;
        }

        public void setDificultad(string df)
        {
            dificultad = df;
        }
        public string getDificultad()
        {
            return dificultad;
        }
    }
}
