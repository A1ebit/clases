﻿using Practica_Clases;
using System;

namespace Practica_Clases
{
    class Programa
    {
        static void Main(string[] args)
        {     
            try
            {
                Profesor Perez = new Profesor("fisica", 5, "tema 1", "cuaderno", "facil");
                Perez.clase();
                Perez.alago("bastante amable");
                Console.WriteLine(Perez.ToString());

                Console.WriteLine("");
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("");

                Profesor Garcia = new Profesor("fisica", 6, "tema 2", "cuaderno", "facil");
                Garcia.setDificultad("dificil");
                Garcia.setEquipo("libro");
                Garcia.setHoras(3);
                Garcia.setMateria("castellano");
                Garcia.setTema("tema 3");
                Console.WriteLine(Garcia.ToString());

                Console.WriteLine("");
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("");

                Suplente James = new Suplente("programacion", 5, "tema 2", "pc", "llorar", "bastante");
                James.clase();
                Console.WriteLine(James.ToString());
            }
            catch (FormatException a)
            {
                Console.WriteLine("formatexeption " + a.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("exeption " + e.Message);
            }
            finally
            {
                Console.WriteLine("");
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("Finalizando programa");
            }
        }
    }
}
