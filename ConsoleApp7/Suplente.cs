﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_Clases
{
    class Suplente:Profesor
    {
        private string experiencia;
        public Suplente (string mat, int hor, string tem, string equip, string dif, string exp) : base (mat, hor, tem, equip, dif)
        {
            experiencia = exp;
            if (exp!="ninguna" && exp != "poca" && exp != "considerable" && exp != "bastante")
            {
                throw new Exception("La experiencia no queda clara");
            }
        }
        public override void clase()
        {
            Console.WriteLine("Hola, muy buenas soy su prof. suplente");
        }
        public void setExperiencia(string exp2)
        {
            experiencia = exp2;
        }
        public string getExperiencia()
        {
            return experiencia;
        }
        public override string ToString()
        {
            return "El prof. de " + materia + " da " + horas + " h al dia, y esta con el tema de " + tema + ", lo que usamos en sus clases son " + equipo + " y sentimos que su clase es " + dificultad + " y su experiencia como prof es " + experiencia;
        }
    }
}
